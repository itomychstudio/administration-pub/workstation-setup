#!/bin/bash

SOURCE=${BASH_SOURCE[0]}
while [ -h "$SOURCE" ]; do
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )

clear
echo "Progress |*--------------|"

curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/.api' --output $DIR/".api"
secret_key=$(cat $DIR/.api | openssl aes-256-cbc -d -a -pass pass:hesoyam)
mkdir $DIR/Updates
clear
echo "Progress |-*-------------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/Automation.sh' --output $DIR/Updates/Automation.sh
clear
echo "Progress |--*------------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/bash.bash' --output $DIR/Updates/bash.bash
clear
echo "Progress |---*-----------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/user_management.bash' --output $DIR/Updates/user_management.bash
clear
echo "Progress |----*----------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/create_user.bash' --output $DIR/Updates/create_user.bash
clear
echo "Progress |-----*---------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/hide_user.bash' --output $DIR/Updates/hide_user.bash
clear
echo "Progress |------*--------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/programms.bash' --output $DIR/Updates/programms.bash
clear
echo "Progress |-------*-------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/transfer_user.bash' --output $DIR/Updates/transfer_user.bash
clear
echo "Progress |--------*------|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/version.bash' --output $DIR/Updates/version.bash
clear
echo "Progress |---------*-----|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/README.txt' --output $DIR/Updates/README.txt
clear
echo "Progress |----------*----|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/update.bash' --output $DIR/Updates/update.bash
clear
echo "Progress |-----------*---|"
curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/modules.bash' --output $DIR/Updates/modules.bash
clear
echo "Progress |------------*--|"
FILEID_GDRIVE="1YLi0NXQg_bS03qnGG2ABm0elM1FrRqIv"
cd $DIR/Updates
curl -s -L "https://www.googleapis.com/drive/v3/files/${FILEID_GDRIVE}?alt=media&key=${secret_key}" > $DIR/Updates/Automation.zip
sudo chown -R ${whoami} $DIR/Updates
unzip -qq $DIR/Updates/Automation.zip
yes | sudo rm -rf $DIR/Automation.app
mv -v $DIR/Updates/Automation.app $DIR/
clear
echo "Progress |-------------*-|"
yes | sudo rm -rf $DIR/Updates/"__MACOSX"
yes | sudo rm -rf $DIR/Updates/Automation.zip
sudo chmod -R 777 $DIR/Updates
mv -v $DIR/Updates/* $DIR
clear
echo "Progress |--------------*|"
yes | sudo rm -rf $DIR/Updates
xattr -d -r com.apple.quarantine $DIR/Automation.app
sudo bash $DIR/Automation.sh