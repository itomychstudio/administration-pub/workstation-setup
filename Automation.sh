#!/usr/bin/env bash

printf '\e[8;10;76t'
SOURCE=${BASH_SOURCE[0]}
while [ -h "$SOURCE" ]; do
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )

yes | sudo rm -rf /tmp/Auto
mkdir /tmp/Auto
touch /tmp/Auto/ver.txt
#Change version in the next alias
APP_VERSION=1.35
echo $APP_VERSION > /tmp/Auto/ver.txt
sudo chmod -R 777 /tmp
sudo chmod -R 777 $DIR

echo "Make your choise: "
clear
programms_color=$( echo -e "\033[1;31m1.\033[0mInstall programms" )     #1
modules_color=$( echo -e "\033[1;31m2.\033[0mInstall modules" )         #2
user_color=$( echo -e "\033[1;31m3.\033[0mUser management" )            #3
mdm_color=$( echo -e "\033[1;31m4.\033[0mMDM Profiles" )                #4
updates_color=$( echo -e "\033[1;31m5.\033[0mCheck for Updates" )       #5
help_color=$( echo -e "\033[1;31m6.\033[0mHelp" )                       #6
quit_color=$( echo -e "\033[1;31m7.\033[0mQuit" )                       #7
main="$programms_color $modules_color $user_color $mdm_color" 
main2=" $updates_color $help_color $quit_color"

echo $main
echo $main2
echo "Make your choise: "
read CHOISE_MAIN
if [[ "${CHOISE_MAIN}" == "1" ]]; then
        $DIR/programms.bash
        clear
        echo $main
elif [[ "${CHOISE_MAIN}" == "2" ]]; then
        bash $DIR/modules.bash
elif [[ "${CHOISE_MAIN}" == "3" ]]; then
        $DIR/user_management.bash
elif [[ "${CHOISE_MAIN}" == "4" ]]; then
        open https://mdm.itomy.ch/mydevices
        clear
        echo "Going to MDM website"
        sleep 5
        wait
    sudo bash $DIR/Automation.sh
elif [[ "${CHOISE_MAIN}" == "5" ]]; then
        clear
        echo "Checking Update..."
        curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/version.bash' > $DIR/version.bash
        sleep 3
        wait
        sudo chmod 777 $DIR/version.bash
        $DIR/version.bash
elif [[ "${CHOISE_MAIN}" == "6" ]]; then
    clear
    echo "This script was created to automate routine tasks.
(All files should be in the same folder)
    Created by Vladyslav Avramenko / Omega
    Email: vladyslav.avramenko@itomy.ch
    Telegram: @Omega640
    version $APP_VERSION"
    echo " "
    echo "(Press ENTER (Return) to continue)
"
    read WAITING
    sudo bash $DIR/Automation.sh
elif [[ "${CHOISE_MAIN}" == "7" ]]; then
    yes | sudo rm -rf /tmp/Auto
    clear
    osascript -e 'tell application "Terminal" to quit'
    killall Terminal
else
        clear
        echo "Invalid option, reply"
        sleep 2
        wait
        $DIR/Automation.sh
fi