#!/bin/bash

SOURCE=${BASH_SOURCE[0]}
while [ -h "$SOURCE" ]; do
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )

clear
echo "Checking Update"
APP_VERSION=$(cat /tmp/Auto/ver.txt)
if [[ $APP_VERSION != "1.35" ]]; then
    curl -s 'https://gitlab.com/itomychstudio/administration-pub/workstation-setup/-/raw/main/update.bash' --output $DIR/update.bash
    sleep 1
    wait
    sudo chmod 777 $DIR/update.bash
    $DIR/update.bash
else
clear
echo "You have the latest version $APP_VERSION"
sleep 5
wait
sudo bash $DIR/Automation.sh
fi