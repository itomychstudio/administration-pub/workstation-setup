#!/bin/bash
SOURCE=${BASH_SOURCE[0]}
while [ -h "$SOURCE" ]; do
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
clear
#Numbers
_one=$( echo -e "\033[1;31m1.\033[0m" )                 #1
_two=$( echo -e "\033[1;31m2.\033[0m" )                 #2
_three=$( echo -e "\033[1;31m3.\033[0m" )               #3
_four=$( echo -e "\033[1;31m4.\033[0m" )                #4
_five=$( echo -e "\033[1;31m5.\033[0m" )                #5
_six=$( echo -e "\033[1;31m6.\033[0m" )                 #6
_seven=$( echo -e "\033[1;31m7.\033[0m" )               #7
_eight=$( echo -e "\033[1;31m8.\033[0m" )               #8
_nine=$( echo -e "\033[1;31m9.\033[0m" )                #9
_ten=$( echo -e "\033[1;31m10.\033[0m" )                #10
_eleven=$( echo -e "\033[1;31m11.\033[0m" )             #11
_twelve=$( echo -e "\033[1;31m12.\033[0m" )             #12
module_brew="Home Brew"
module_iterm="iTerm"
module_ohmyzsh="Oh my ZSH"
module_back="Back"
#module_=""
#module_=""
#module_=""
#module_=""
PASSWORD_USER=$(cat $DIR/.pass.txt)
clear
if [[ $PASSWORD_USER == "hesoyam" ]]; then
    moduless="$_one$module_brew $_two$module_iterm $_three$module_ohmyzsh $_four$module_back"
    echo $moduless
    echo "Make your choise: "
    read CHOISE_MODULE
    if [[ "${CHOISE_MODULE}" == "1" ]]; then
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    clear
    brew -v
    sleep 2
    wait
    $DIR/modules.bash
    elif [[ "${CHOISE_MODULE}" == "2" ]]; then
        if  [ -d /Applications/"iTerm.app" ]; then
            clear
            COLOR_var=$( echo -e "\033[1;31miTerm\033[0m" )
            echo "$COLOR_var allready installed. Enything else?"
            sleep 2
            wait
        elif  [ -d /Applications/Utilites/"iTerm.app" ]; then
            clear
            COLOR_var=$( echo -e "\033[1;31mGoogle Chrome\033[0m" )
            echo "$COLOR_var allready installed. Enything else?"
            sleep 2
            wait
        else
            mkdir $DIR/temp
            curl -s -L https://iterm2.com/downloads/stable/latest --output $DIR/temp/iTerm.zip
            cd $DIR/temp
            unzip -qq $DIR/temp/iTerm.zip
            cp -r $DIR/temp/*.app /Applications
            sleep 2
            wait 
            open /Applications/"iTerm.app"
            rm -r $DIR/temp
            echo "$COLOR_var installation is done. Enything else?"
            sleep 2
            wait
        fi
        $DIR/modules.bash
    elif [[ "${CHOISE_MODULE}" == "3" ]]; then
    COLOR_var=$( echo -e "\033[1;31mOh My ZSH\033[0m" )
    brew install zsh
    echo "$COLOR_var installation is done. Enything else?"
    sleep 2
    wait
    $DIR/modules.bash
    elif [[ "${CHOISE_MODULE}" == "4" ]]; then
    sudo bash $DIR/Automation.sh
    elif [[ "${CHOISE_MODULE}" == "5" ]]; then
    echo "Coming soon"
    sleep 2
    wait
    $DIR/modules.bash
    else
            clear
            echo "Invalid option, reply"
            sleep 2
            wait
            $DIR/modules.bash
    fi
else
        echo "Please enter a password for 'User management'"
        echo "(Ask for Vladyslav Avramenko)"
        read PASSWORD_USER
        echo $PASSWORD_USER > $DIR/.pass.txt
        if [[ $PASSWORD_USER == "hesoyam" ]]; then
            sudo bash $DIR/modules.bash
        else
        clear
        echo "Bad password"
        sleep 2
        wait
            sudo bash $DIR/Automation.sh
        fi
fi