#!/bin/bash
SOURCE=${BASH_SOURCE[0]}
while [ -h "$SOURCE" ]; do
  DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
  SOURCE=$(readlink "$SOURCE")
  [[ $SOURCE != /* ]] && SOURCE=$DIR/$SOURCE
done
DIR=$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )
        clear
        #Numbers
        _one=$( echo -e "\033[1;31m1.\033[0m" )                 #1
        _two=$( echo -e "\033[1;31m2.\033[0m" )                 #2
        _three=$( echo -e "\033[1;31m3.\033[0m" )               #3
        _four=$( echo -e "\033[1;31m4.\033[0m" )                #4
        _five=$( echo -e "\033[1;31m5.\033[0m" )                #5
        _six=$( echo -e "\033[1;31m6.\033[0m" )                 #6
        _seven=$( echo -e "\033[1;31m7.\033[0m" )               #7
        _eight=$( echo -e "\033[1;31m8.\033[0m" )               #8
        _nine=$( echo -e "\033[1;31m9.\033[0m" )                #9
        _ten=$( echo -e "\033[1;31m10.\033[0m" )                #10
        _eleven=$( echo -e "\033[1;31m11.\033[0m" )             #11
        _twelve=$( echo -e "\033[1;31m12.\033[0m" )             #12
        #Words
        prog_chrome="Google Chrome"                             #1
        prog_slack="Slack"                                      #2
        prog_anydesk="AnyDesk"                                  #3
        prog_telegram="Telegram"                                #4
        prog_all=$( echo -e  "All (\033[1;31m1-4\033[0m)" )     #5 
        prog_openvpn="OpenVPN"                                  #6
        prog_sketch="Sketch"                                    #7
        prog_sublime="Sublime"                                  #8
        prog_skype="Skype"                                      #9
        prog_link="Link to file"                                #10
        prog_path="Path to file"                                #11
        prog_back="Back"                                        #12
        secret_key=$(cat $DIR/.api | openssl aes-256-cbc -d -a -pass pass:hesoyam)
        progs="$_one$prog_chrome $_two$prog_slack $_three$prog_anydesk $_four$prog_telegram $_five$prog_all $_six$prog_openvpn $_seven$prog_sketch $_eight$prog_sublime $_nine$prog_skype $_ten$prog_link $_eleven$prog_path $_twelve$prog_back"
        echo $progs
        echo "Make your choise: "
        read CHOISE_PROGS
        if [[ "${CHOISE_PROGS}" == "1" ]]; then
                    if  [ -d /Applications/"Google Chrome.app" ]; then
                        clear
                        COLOR_var=$( echo -e "\033[1;31mGoogle Chrome\033[0m" )
                        echo "$COLOR_var allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Progress |*----|"
                        if [[ `arch` == arm64 ]]; then
                            curl -s 'https://dl.google.com/chrome/mac/universal/stable/GGRO/googlechrome.dmg' > $DIR/temp/1.dmg
                        else
                            curl -s 'https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg' > $DIR/temp/1.dmg
                        fi
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Progress |----*|"
                        echo "$COLOR_var installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "2" ]]; then
                    if  [ -d /Applications/"Slack.app" ]; then
                        clear
                        COLOR_var=$( echo -e "\033[1;31mSlack\033[0m" )
                        echo "$COLOR_var allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Progress |*----|"
                        curl -s 'https://downloads.slack-edge.com/releases/macos/4.23.0/prod/universal/Slack-4.23.0-macOS.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Progress |----*|"
                        echo "$COLOR_var installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "3" ]]; then
                    if  [ -d /Applications/"AnyDesk.app" ]; then
                        clear
                        COLOR_var=$( echo -e "\033[1;31mAnyDesk\033[0m" )
                        echo "$COLOR_var allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Progress |*----|"
                        curl -s 'https://download.anydesk.com/anydesk.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Progress |----*|"
                        open x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility
                        echo "$COLOR_var installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "4" ]]; then
                    if  [ -d /Applications/"Telegram.app" ]; then
                        clear
                        COLOR_var=$( echo -e "\033[1;31mTelegram\033[0m" )
                        echo "$COLOR_var allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Progress |*----|"
                        curl -s 'https://osx.telegram.org/updates/Telegram.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Progress |----*|"
                        echo "$COLOR_var installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "5" ]]; then
                    clear
                    if  [ -d /Applications/"Google Chrome.app" ]; then
                        COLOR_var=$( echo -e "\033[1;31mGoogle Chrome\033[0m" )
                        echo "$COLOR_var allready installed."
                    else
                        echo "Installing Google Chrome"
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Google Chrome Progress |*----|"
                        if [[ `arch` == arm64 ]]; then
                            curl -s 'https://dl.google.com/chrome/mac/universal/stable/GGRO/googlechrome.dmg' > $DIR/temp/1.dmg
                        else
                            curl -s 'https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg' > $DIR/temp/1.dmg
                        fi
                        clear
                        echo "Google Chrome Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Google Chrome Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Google Chrome Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "$COLOR_var installation is done."
                    fi

                    if  [ -d /Applications/"Slack.app" ]; then
                        COLOR_var=$( echo -e "\033[1;31mSlack\033[0m" )
                        echo "$COLOR_var allready installed."
                    else
                        echo "Installing Slack"
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Google Chrome installation is done."
                        echo "Slack Progress |*----|"
                        curl -s 'https://downloads.slack-edge.com/releases/macos/4.23.0/prod/universal/Slack-4.23.0-macOS.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Google Chrome installation is done."
                        echo "Slack Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Google Chrome installation is done."
                        echo "Slack Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Google Chrome installation is done."
                        echo "Slack Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Google Chrome installation is done."
                        echo "$COLOR_var installation is done."
                    fi

                    if  [ -d /Applications/"Telegram.app" ]; then
                        COLOR_var=$( echo -e "\033[1;31mTelegram\033[0m" )
                        echo "$COLOR_var allready installed."
                    else
                        echo "Installing Telegram"
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram Progress |*----|"
                        curl -s 'https://osx.telegram.org/updates/Telegram.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram is done"
                    fi

                    if  [ -d /Applications/"AnyDesk.app" ]; then
                        COLOR_var=$( echo -e "\033[1;31mAnyDesk\033[0m" )
                        echo "$COLOR_var allready installed."
                    else
                        echo "Installing AnyDesk"
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram is done"
                        echo "AnyDesk Progress |*----|"
                        curl -s 'https://download.anydesk.com/anydesk.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram is done"
                        echo "AnyDesk Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram is done"
                        echo "AnyDesk Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram is done"
                        echo "AnyDesk Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        clear
                        echo "Google Chrome installation is done"
                        echo "Slack installation is done."
                        echo "Telegram is done"
                        echo "AnyDesk is done"
                        open x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility
                    fi

                    echo "$prog_all installations are done. Enything else?"
                    echo "----------------------------------------------------------------------------"
                    echo $progs
                    sleep 4
                    wait
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "6" ]]; then
                    if  [ -d /Applications/"OpenVpn Connect"/"OpenVPN Connect.app" ]; then
                        clear
                        echo "$prog_openvpn allready installed"
                        sleep 2
                        wait
                    else
                        clear
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Progress |*----|"
                        curl -s 'https://swupdate.openvpn.net/downloads/connect/openvpn-connect-3.3.3.4163_signed.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*--|"
                        sudo installer -pkg $DIR/temp/mount/*.pkg -target /Applications >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |----*|"
                        rm -r $DIR/temp
                        echo "$prog_openvpn installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "7" ]]; then
                    if  [ -d /Applications/"Sketch.app" ]; then
                        clear
                        echo "$prog_sketch allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        mkdir -p $DIR/temp/mount
                        mkdir -p $DIR/temp/mount1
                        #SKETCH Google Drive file ID
                        FILEID_GDRIVE="1KEuOT_dm9LTLdneZWU6vg0SK-Iy8cR3X"
                        clear
                        echo "Progress |*----|"
                        curl -s -L "https://www.googleapis.com/drive/v3/files/${FILEID_GDRIVE}?alt=media&key=${secret_key}" > $DIR/temp/Sketch.dmg
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/Sketch.dmg
                        clear
                        echo "Progress |--*--|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount1 $DIR/temp/mount/"Manual Install"/*.dmg
                        clear
                        echo "Progress |---*-|"
                        cp -r $DIR/temp/mount1/*.app /Applications
                        hdiutil detach $DIR/temp/mount1
                        hdiutil detach $DIR/temp/mount
                        rm -r $DIR/temp
                        clear
                        echo "Progress |----*|"
                        xattr -d -r com.apple.quarantine /Applications/Sketch.app/
                        echo "$prog_sketch installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "8" ]]; then
                    if  [ -d /Applications/"Sublime Text.app" ]; then
                        clear
                        echo "$prog_sublime allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        mkdir -p $DIR/temp/mount
                        mkdir -p $DIR/temp/mount1
                        #SUBLIME Google Drive file ID
                        FILEID_GDRIVE="1eP3wJ1hlPY04Sc6kxlONQSyjwraXlcjv"
                        clear
                        echo "Progress |*-----|"
                        curl -s -L "https://www.googleapis.com/drive/v3/files/${FILEID_GDRIVE}?alt=media&key=${secret_key}" > $DIR/temp/Sketch.dmg
                        clear
                        echo "Progress |-*----|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/Sublime.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount1 $DIR/temp/mount/"Manual Install"/*.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |---*--|"
                        cp $DIR/temp/mount1/Extra/"Sublime Text Lic.txt" $DIR/temp
                        open $DIR/temp/"Sublime Text Lic.txt"
                        cp -r $DIR/temp/mount1/*.app /Applications
                        xattr -d -r com.apple.quarantine /Applications/"Sublime Text.app"/
                        clear
                        echo "Progress |----*-|"
                        clear
                        hdiutil detach $DIR/temp/mount1 >> /tmp/Auto/log.txt
                        echo "Progress |-----*|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        rm -r $DIR/temp
                        echo "$prog_sublime installation is done. Please past license key "Help - Enter License". Enything else?"
                        sleep 3
                        wait
                        open /Applications/"Sublime Text.app"
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "9" ]]; then
                    if  [ -d /Applications/"Skype.app" ]; then
                        clear
                        echo "$prog_skype allready installed. Enything else?"
                        sleep 2
                        wait
                    else
                        echo "Installing $prog_skype"
                        mkdir -p $DIR/temp/mount
                        clear
                        echo "Progress |*----|"
                        curl -s 'https://download.skype.com/s4l/download/mac/Skype-8.80.0.141.dmg' > $DIR/temp/1.dmg
                        clear
                        echo "Progress |-*---|"
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount $DIR/temp/1.dmg >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |--*--|"
                        cp -r $DIR/temp/mount/*.app /Applications
                        clear
                        echo "Progress |---*-|"
                        hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                        clear
                        echo "Progress |----*|"
                        rm -r $DIR/temp
                        echo "$prog_skype installation is done. Enything else?"
                        sleep 2
                        wait
                    fi
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "10" ]]; then
                    mkdir -p $DIR/temp/mount
                    mkdir -p $DIR/temp/mount1
                    mkdir -p $DIR/temp/mount2
                    clear
                    echo "Paste weblink 'https://*****/*****/*.***|' for installation:"
                    read varlink
                    clear
                    echo "Installing your app"
                    curl -s --create-dirs -O --output-dir $DIR/temp/mount/ $varlink
                    if [ $DIR/temp/mount/*.* == $DIR/temp/mount/*.app ]; then
                        cp -r $DIR/temp/mount/*.app /Applications
                    elif [ $DIR/temp/mount/*.* == $DIR/temp/mount/*.dmg ]; then
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount1 $DIR/temp/mount/*.dmg >> /tmp/Auto/log.txt
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount2 $DIR/temp/mount1/"Manual Install"/*.dmg >> /tmp/Auto/log.txt
                        cp -r $DIR/temp/mount2/*.app /Applications
                    elif [ $DIR/temp/mount/*.* == $DIR/temp/mount/*.pkg ]; then
                        sudo installer -pkg $DIR/temp/mount/*.pkg -target /Applications >> /tmp/Auto/log.txt
                    fi
                    cp -r $DIR/temp/mount1/*.app /Applications
                    hdiutil detach $DIR/temp/mount2 >> /tmp/Auto/log.txt
                    hdiutil detach $DIR/temp/mount1 >> /tmp/Auto/log.txt
                    hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                    rm -r $DIR/temp
                    clear
                    echo "Your path installation is done. Enything else?"
                    sleep 2
                    wait
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "11" ]]; then
                    mkdir -p $DIR/temp/mount
                    mkdir -p $DIR/temp/mount1
                    mkdir -p $DIR/temp/mount2
                    echo "Paste path '/*****/*.***|' for your installation file:"
                    read varpath
                    echo "Installing your app"
                    cp $varpath $DIR/temp/mount/
                    if [ $DIR/temp/mount/*.* == $DIR/temp/mount/*.app ]; then
                        cp -r $DIR/temp/mount/*.app /Applications
                    elif [ $DIR/temp/mount/*.* == $DIR/temp/mount/*.dmg ]; then
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount1 $DIR/temp/mount/*.dmg >> /tmp/Auto/log.txt
                        yes | hdiutil attach -noverify -nobrowse -mountpoint $DIR/temp/mount2 $DIR/temp/mount1/"Manual Install"/*.dmg >> /tmp/Auto/log.txt
                        cp -r $DIR/temp/mount2/*.app /Applications
                    elif [ $DIR/temp/mount/*.* == $DIR/temp/mount/*.pkg ]; then
                        sudo installer -pkg $DIR/temp/mount/*.pkg -target /Applications >> /tmp/Auto/log.txt
                    fi
                    cp -r $DIR/temp/mount1/*.app /Applications
                    hdiutil detach $DIR/temp/mount2 >> /tmp/Auto/log.txt
                    hdiutil detach $DIR/temp/mount1 >> /tmp/Auto/log.txt
                    hdiutil detach $DIR/temp/mount >> /tmp/Auto/log.txt
                    rm -r $DIR/temp
                    clear
                    echo "Your path installation is done. Enything else?"
                    sleep 2
                    wait
                    echo $progs
                    $DIR/programms.bash
        elif [[ "${CHOISE_PROGS}" == "12" ]]; then
                    sudo bash $DIR/Automation.sh
        else
            echo "Invalid option, reply"
            sleep 2
            wait
            $progs
            $DIR/programms.bash
        fi